import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImmobileComponent } from './immobile.component';
import { ImmobileHomeComponent } from './immobile-home/immobile-home.component';

//Rotas
export const ImmobileRoutes: Routes = [
    {path: 'immobile', component: ImmobileComponent,
        children:[
            {path: '', redirectTo: '/immobile/immobile-home', pathMatch: 'full'},
            {path: 'immobile-home', component: ImmobileHomeComponent}
        ]
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(ImmobileRoutes)],
    exports: [ RouterModule ]
})

export class ImmobileRoutingModule { }