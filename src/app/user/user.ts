export class User {
    id: number;
    nomeCompleto: string;
    email: string;
    tipodePessoaEnum: string;
    cpf: number;
    cnpj: number;
    senha: string;
    tipoDeUsuarioEnum: string;
}