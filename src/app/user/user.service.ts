import { Injectable, ErrorHandler } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from './user';

@Injectable()
export class UserService {

    private API_APPLE = 'http://localhost:8080'

    constructor(private http: Http) { }

    save(user: User): Observable<any> {

        let headers = new Headers({ 'Content-Type': 'application/json' })
        let options = new RequestOptions({ headers: headers })

        const body = JSON.stringify(user);

        return this.http.post(`${this.API_APPLE}/user`, body, options)
            .map(response => response.json());
    }
}
