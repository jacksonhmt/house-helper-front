import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-immobile',
  templateUrl: './immobile-home.component.html',
  styleUrls: ['./immobile-home.component.css']
})
export class ImmobileHomeComponent implements OnInit {

  constructor(private router: Router, private titleService: Title) { }

  ngOnInit() {
   this.titleService.setTitle('Immobile - House Helper')
  }

}
