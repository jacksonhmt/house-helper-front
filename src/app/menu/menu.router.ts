import { Routes, RouterModule } from '@angular/router';

import { MenuComponent } from './menu.component';
import { HomeComponent } from './../home/home.component';
import { NgModule } from '@angular/core';
import { ImmobileRoutes } from '../immobile/immobile.router';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component'
import { UserComponent } from '../user/user.component';

// Rotas
export const MenuRoutes: Routes = [
    {
        path: '',
        component: MenuComponent,
        children: [
            { path: '', redirectTo: '/home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'user', component: UserComponent },
            { path: '**', component: PageNotFoundComponent }
        ]
    },
    ...ImmobileRoutes
]

@NgModule({
    imports: [RouterModule.forRoot(MenuRoutes)],
    exports: [RouterModule]
})

export class MenuRoutingModule { }
