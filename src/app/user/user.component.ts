import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent implements OnInit {

  isFisica: Boolean = true;
  userForm: FormGroup;
  user: User;
  cnpj: boolean;
  cpf: boolean;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private builder: FormBuilder,
  ) { }

  ngOnInit() {
    this.userForm = this.builder.group({
      id: [],
      nomeCompleto: ['', [Validators.required]],
      email: ['', [Validators.required]],
      tipodePessoaEnum: ['', Validators.required],
      cpf: ['', [Validators.required]],
      cnpj: ['', [Validators.required]],
      senha: ['', [Validators.required]],
      tipoDeUsuarioEnum: ['', [Validators.required]],
    }, {});
  }

  mudarCampo() {
    this.isFisica = this.userForm.value.tipodePessoaEnum == 0;
  }

  save(user: User) {
    this.userService.save(user).subscribe(data => {
      this.router.navigate(['/']);
    })
  }

  private handleError(err: any): Promise<any> {
    return Promise.reject(err.message || err)
  }
}

