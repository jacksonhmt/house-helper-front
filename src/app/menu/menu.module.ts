import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './../home/home.component';
import { MenuComponent } from './menu.component';
import { ImmobileModule } from './../immobile/immobile.module';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { FavoritesComponent } from '../favorites/favorites.component';
import { UserService } from '../user/user.service';
import { UserComponent } from '../user/user.component';

@NgModule({
  imports: [  
    HttpModule,
    HttpClientModule,
    CommonModule,
    RouterModule,
    AngularFontAwesomeModule,
    ImmobileModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    MenuComponent,
    HomeComponent,
    PageNotFoundComponent,
    FavoritesComponent,
    UserComponent,
  ],
  providers:[
    UserService
  ]
})
export class MenuModule { }
