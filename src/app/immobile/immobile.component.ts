import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-immobile',
  templateUrl: './immobile.component.html',
  styleUrls: ['./immobile.component.css']
})
export class ImmobileComponent implements OnInit {

  constructor(private router: Router, private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('immobile - House Helper')
  }

}
