import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { ImmobileComponent } from './immobile.component';
import { ImmobileRoutingModule } from './immobile.router';
import { ImmobileHomeComponent } from './immobile-home/immobile-home.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ImmobileRoutingModule,
    AngularFontAwesomeModule
  ],
  declarations: [
    ImmobileComponent,
    ImmobileHomeComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ImmobileModule { }
