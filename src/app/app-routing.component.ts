import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuRoutes } from './menu/menu.router';

export const routes: Routes = [
    ...MenuRoutes
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(routes);